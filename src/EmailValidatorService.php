<?php

namespace Drupal\email_validator_service;

use Drupal\Component\Utility\EmailValidatorInterface;
use Egulias\EmailValidator\EmailValidator as EmailValidatorUtility;
use Egulias\EmailValidator\Validation\EmailValidation;
use Egulias\EmailValidator\Validation\RFCValidation;
use Egulias\EmailValidator\Validation\DNSCheckValidation;
use Egulias\EmailValidator\Validation\MultipleValidationWithAnd;

/**
 * Validates email addresses.
 */
class EmailValidatorService extends EmailValidatorUtility implements EmailValidatorInterface {

  /**
   * {@inheritdoc}
   */
  public function isValid($email, EmailValidation $email_validation = NULL) {
    if ($email_validation) {
      throw new \BadMethodCallException('Calling \Drupal\Component\Utility\EmailValidator::isValid() with the second argument is not supported. See https://www.drupal.org/node/2997196');
    }

    return parent::isValid($email, (new MultipleValidationWithAnd([(new RFCValidation), (new DNSCheckValidation)])));
  }

}
