INTRODUCTION
------------
Add an email validator service to perform an additional DNS check on emails.
The benefit of having a service is it should work out of the box with all Drupal email fields.


REQUIREMENTS
------------
This module does not have any dependency on any other module.


INSTALLATION
------------
Install as normal (see http://drupal.org/documentation/install/modules-themes).


CONFIGURATION
-------------
This module does not have any configuration.
